Feature: Afficher un theme avec differents objets de navigation

  Scenario Outline: Affichages multiples
    Given un butineur est ouvert
    When je suis sur le site
    And je choisis <Sousmenu> dans le menu principal
    And je recherche le theme nomme <Element>
    And je clique sur le theme <Element> dans la liste de resultats
    And je choisis dafficher le site de demonstration du theme
    Then je constate laffichage au format Bureau du texte <TexteDuTheme>
    And je constate laffichage au format Tablette du texte <TexteDuTheme>
    And je ferme laffichage de demonstration
    And je ferme le butineur

    @AdminProfile
    Examples:
      | Sousmenu           | Element  | TexteDuTheme                  |
      | "Thèmes WordPress" | "Tronar" | "The beauty of November lies" |
      | "Thèmes WordPress" | "Pomme"  | "As the 19th century folded"  |