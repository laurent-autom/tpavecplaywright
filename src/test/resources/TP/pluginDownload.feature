Feature: Télécharger une extension

  Scenario Outline: Affichages multiples
    Given un butineur est ouvert
    When je suis sur le site
    And je choisis <Sousmenu> dans le menu principal
    And je recherche lextension nommee <Element>
    And je clique sur lelement <Element> dans la liste de resultats
    Then je clique sur le bouton telecharger
    And je ferme le butineur

    @AdminProfile
    Examples:
      | Sousmenu               | Element                                      |
      | "Extensions WordPress" | "Schema & Structured Data for WP & AMP"      |
      | "Extensions WordPress" | "Advanced Shipment Tracking for WooCommerce" |