package org.ecoleql;

import com.microsoft.playwright.*;
import com.microsoft.playwright.options.AriaRole;
import com.microsoft.playwright.options.LoadState;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;

import java.nio.file.Paths;

import static com.microsoft.playwright.assertions.PlaywrightAssertions.assertThat;

public class Steps {
    String Josselin = "C:\\Users\\Formation\\Desktop\\TPavecPlayWright";
    Playwright playwright = Playwright.create();
    Browser butineur = playwright.firefox().launch(new BrowserType.LaunchOptions().setHeadless(false));
    Page wordpress = butineur.newPage();

    @Given("un butineur est ouvert")
    public void butineurLaunch() {
        wordpress.navigate("https://wordpress.com/fr/");
    }

    @And("je suis sur le site")
    public void websiteIsLive() {
        assertThat(wordpress).hasTitle("WordPress.com : créez un site, vendez vos produits, lancez un blog et bien plus encore");

        // Gestion des cookies
        wordpress.getByRole(AriaRole.LINK, new Page.GetByRoleOptions().setName("Accepter tout")).click();
    }

    @And("je choisis {string} dans le menu principal")
    public void gestionMenuPrincipal(String sousmenu) {
        wordpress.getByRole(AriaRole.MENUITEM, new Page.GetByRoleOptions().setName("Fonctionnalités")).click();
        wordpress.getByRole(AriaRole.MENUITEM, new Page.GetByRoleOptions().setName(sousmenu)).click();
    }

    @And("je recherche lextension nommee {string}")
    public void rechercheExtension(String element) {
        // Nécessité de cliquer dans le champ de recherche avant d'interagir avec
        wordpress.getByPlaceholder("Essayez de rechercher").click();
        wordpress.getByPlaceholder("Essayez de rechercher").fill(element);
        wordpress.getByLabel("Ouvrir la recherche").click();
    }

    @And("je clique sur lelement {string} dans la liste de resultats")
    public void rechercheExtensionGestionResultats(String element) {
        wordpress.getByRole(AriaRole.LINK, new Page.GetByRoleOptions().setName("plugin-icon "+element)).click();

        // Vérification que nous sommes sur la wordpress de l'extension
        assertThat(wordpress.getByRole(AriaRole.HEADING, new Page.GetByRoleOptions().setName(element))).hasText(element);
    }

    @Then("je clique sur le bouton telecharger")
    public void downloadExtension() {
        Download download = wordpress.waitForDownload(() -> {
            wordpress.getByRole(AriaRole.LINK, new Page.GetByRoleOptions().setName("Télécharger").setExact(true)).click();
        });
        // Josselin, n'oublie pas de préciser le chemin absolu en paramètre en haut, si tu veux tester
        download.saveAs(Paths.get(Josselin, download.suggestedFilename()));
    }

    @And("je recherche le theme nomme {string}")
    public void rechercheThemes(String theme) {
        wordpress.waitForLoadState(LoadState.DOMCONTENTLOADED);
        wordpress.getByPlaceholder("Recherche parmi les thèmes...").click();
        wordpress.getByPlaceholder("Recherche parmi les thèmes...").fill(theme);
        // Simulation de pression d'une touche du clavier
        wordpress.getByPlaceholder("Recherche parmi les thèmes...").press("Enter");
    }

    @And("je clique sur le theme {string} dans la liste de resultats")
    public void rechercheThemesGestionResultats(String theme) {
        wordpress.getByLabel(theme, new Page.GetByLabelOptions().setExact(true)).click();

        // Vérification que nous sommes sur la page du thème
        assertThat(wordpress.getByRole(AriaRole.HEADING, new Page.GetByRoleOptions().setName(theme))).hasText(theme);
    }

    @And("je choisis dafficher le site de demonstration du theme")
    public void themeDemonstration() {
        wordpress.getByRole(AriaRole.BUTTON, new Page.GetByRoleOptions().setName("Site de démo").setExact(true)).click();
    }

    @Then("je constate laffichage au format Bureau du texte {string}")
    public void themeDemonstrationAssertionDesktop(String TexteDuTheme) {
        wordpress.frameLocator("iframe[title=\"Aperçu\"] >> nth=1").getByText(TexteDuTheme).click();
    }

    @And("je constate laffichage au format Tablette du texte {string}")
    public void themeDemonstrationAssertioniPad(String TexteDuTheme) {
        // Ici avec un peu de temps j'aurais développé un module qui prend en entrée "Bureau", "Tablette" ou "Tél." et
        // qui aurait cliqué sur le bon élément dans le menu déroulant
        wordpress.getByRole(AriaRole.BUTTON, new Page.GetByRoleOptions().setName("Bureau")).click();
        wordpress.getByLabel("Tablette").click();
        wordpress.frameLocator("iframe[title=\"Aperçu\"] >> nth=1").getByText(TexteDuTheme).click();
    }

    @And("je ferme laffichage de demonstration")
    public void themeDemonstrationFermer() {
        wordpress.getByLabel("Fermer l’aperçu").click();
    }

    @And("je ferme le butineur")
    public void closeButineur() {
        butineur.close();
    }
}
